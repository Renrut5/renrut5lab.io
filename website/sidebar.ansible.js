module.exports = {
  ansible: [
    'getting-started',
    'file-structure',
    {
      type: 'category',
      label: 'Playbooks',
      items: [
        'playbooks/config_interface',
      ],
    },
  ],
};
