module.exports = {
  ansible: [
    'ansible/getting-started',
    'ansible/file-structure',
    {
      type: 'category',
      label: 'Playbooks',
      items: [
        'ansible/playbooks/config_interface',
      ],
    },
  ],
  docs: [
    'getting-started',
    {
      type: 'category',
      label: 'Playbooks',
      items: [
        'getting-started',
        'create-a-page',
        'create-a-document',
        'create-a-blog-post',
        'markdown-features',
        'thank-you',
      ],
    },
  ],
};
