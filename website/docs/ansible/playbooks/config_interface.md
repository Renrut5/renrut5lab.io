---
title: "Playbook: config_interface.yml" 
---

## config_interface.yml
## Input Options
#### host (required)
*  Defines the host to be targeted for the playbook.
*  Entered as an extra variable through the CLI.   
Ex: `-e "host=cisco1`

#### interface (required)
*  Defines which interface on the host to target for update. 
*  Entered as an extra variable through the CLI.   
Ex: `-e "interface="g0/1"`

***

## Example Usage
`$ ansible-playbook config_interface.yml -e 'host=cisco1 interface="g0/1'`
